import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';

function ContainerScrollView(props) {
  return (
    <View style={[styles.viewMain]}>
      <ScrollView
        style={[styles.scrollView]}
        showsVerticalScrollIndicator={false}>
        {props.children}
      </ScrollView>
    </View>
  );
}

export default ContainerScrollView;

const styles = StyleSheet.create({
  viewMain: {flex: 1},
  scrollView: {paddingHorizontal: 15},
});
