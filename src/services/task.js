import {getLists, setLists} from '../configuration/storage';

export const saveTask = async data => {
  const lists = await getLists();
  const targetList = lists.find(list => list.key === data.key);
  if (targetList) {
    targetList.tasks.push(data.task);
    await setLists(lists);
  }
  return true;
};

export const getTaskLists = async data => {
  const lists = await getLists();
  const updatedLists = lists.map(list => {
    const {tasks, ...rest} = list;
    return rest;
  });
  return updatedLists;
};

export const updateTask = async task => {
  const lists = await getLists();
  let taskFound = false;
  lists.forEach(list => {
    list.tasks.forEach(taskToUpdate => {
      if (taskToUpdate.codeTask === task.codeTask) {
        taskToUpdate.taskName = task.taskName;
        taskToUpdate.taskDate = task.taskDate;
        taskToUpdate.done = task.done;
        taskFound = true;
      }
    });
  });
  if (taskFound) {
    await setLists(lists);
    return true;
  } else {
    return false;
  }
};

export const deleteTask = async codeTask => {
  const lists = await getLists();
  let taskDeleted = false;
  lists.forEach(list => {
    const tasks = list.tasks.filter(task => task.codeTask !== codeTask);
    if (tasks.length < list.tasks.length) {
      list.tasks = tasks;
      taskDeleted = true;
    }
  });
  if (taskDeleted) {
    await setLists(lists);
    return true;
  } else {
    return false;
  }
};

export const searchTask = async searchString => {
  const lists = await getLists();
  const matchingTasks = [];

  lists.forEach(list => {
    list.tasks.forEach(task => {
      if (task.taskName.toLowerCase().includes(searchString.toLowerCase())) {
        matchingTasks.push(task);
      }
    });
  });

  return matchingTasks;
};

export const getTasksWithFilter = async (listCode, doneOnly) => {
  const lists = await getLists();

  let tasks = [];

  if (listCode) {
    const foundList = lists.find(list => list.key === listCode);
    tasks = foundList.tasks;
  } else {
    tasks = lists.reduce((acc, curr) => [...acc, ...curr.tasks], []);
  }

  if (doneOnly) {
    tasks = tasks.filter(task => task.done);
  }

  return tasks;
};
