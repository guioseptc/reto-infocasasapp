import {Dimensions} from 'react-native';
import {logger} from 'react-native-logs';

export const log = logger.createLogger();
export const deviceWidth = Dimensions.get('window').width;

export const generateUniqueId = () => {
  const randomStr = Math.random().toString(36).substring(2);
  const timestampStr = Date.now().toString(36);
  return randomStr + timestampStr;
};

export const formatDate = date => {
  const formattedDate = `${date.toLocaleString('default', {
    month: 'short',
  })} ${date.getDate()}, ${date.getFullYear()} ${date.toLocaleTimeString([], {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  })}`;
  return formattedDate;
};
