import AsyncStorage from '@react-native-async-storage/async-storage';
import {log} from './utils';
import {lists} from './constants';

export const setLists = async data => {
  try {
    const stringData = JSON.stringify(data);
    await AsyncStorage.setItem('@INFOCASASAPP_LISTS', stringData);
  } catch (error) {
    log.error('[setLists] ' + error);
    return null;
  }
};

export const getLists = async () => {
  try {
    const data = await AsyncStorage.getItem('@INFOCASASAPP_LISTS');
    return data != null ? JSON.parse(data) : lists;
  } catch (error) {
    log.error('[getLists] ' + error);
    return null;
  }
};

export const removeLists = async () => {
  try {
    await AsyncStorage.removeItem('@INFOCASASAPP_LISTS')
  } catch (error) {
    log.error('[removeLists] ' + error);
    return null;
  }
};
