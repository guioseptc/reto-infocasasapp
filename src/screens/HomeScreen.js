import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {deviceWidth} from '../configuration/utils';
import {getLists, removeLists} from '../configuration/storage';

function HomeScreen(props) {
  const [lists, setLists] = useState([]);

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={[styles.itemTouch]}
      onPress={() =>
        props.navigation.navigate('TasksScreen', {
          title: item.listName,
          data: item,
        })
      }>
      <Text>{item.listName}</Text>
      <View style={[styles.counterTouch]}>
        <Text style={[styles.counterTouchText]}>
          {item.tasks.length}
        </Text>
      </View>
    </TouchableOpacity>
  );

  const getTaskLists = async () => {
    const data = await getLists();
    setLists(data);
  };

  const remove = async () => {
    await removeLists();
  };

  useEffect(() => {
    getTaskLists();
    // reset lists to their initial value
    // remove()
  }, []);

  return (
    <View style={[styles.viewMain]}>
      <View style={[styles.viewSearch]}>
        <TouchableOpacity
          activeOpacity={1}
          style={[styles.viewSearchTouch]}
          onPress={() => props.navigation.navigate('SearchScreen')}>
          <Feather name="search" size={20} color={'#a1a1a1'} />
          <Text style={[styles.viewSearchText]}>Buscar tareas ...</Text>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity
          style={[styles.taskTouch]}
          onPress={() =>
            props.navigation.navigate('TasksScreen', {title: 'Mis tareas'})
          }>
          <Text>{'Mis tareas'}</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={[styles.title]}>Mis listas</Text>
        <View>
          <FlatList
            data={lists}
            numColumns={2}
            renderItem={renderItem}
            keyExtractor={item => item.key}
          />
        </View>
      </View>
    </View>
  );
}

export default HomeScreen;

const styles = StyleSheet.create({
  viewMain: {flex: 1, backgroundColor: 'white', paddingHorizontal: 10},
  viewSearch: {flexDirection: 'row', justifyContent: 'center'},
  viewSearchTouch: {
    backgroundColor: 'rgba(0, 0, 0, 0.03)',
    borderRadius: 25,
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    width: deviceWidth * 0.9,
  },
  viewSearchText: {color: '#a1a1a1', marginLeft: 10, fontSize: 16},
  title: {fontSize: 20, fontWeight: 'bold', margin: 10},
  itemTouch: {
    backgroundColor: 'white',
    flex: 1,
    margin: 6,
    paddingVertical: 30,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 10,
  },
  taskTouch: {
    backgroundColor: 'white',
    margin: 6,
    paddingVertical: 30,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 10,
  },
  counterTouch: {position: "absolute", top: 8, right: 8, backgroundColor: "#FC7B27",
    borderRadius: 20, width: 20, height: 20, flexDirection: "row", alignItems: "center", justifyContent: "center"},
  counterTouchText: {color: "white", fontSize: 12}
});
