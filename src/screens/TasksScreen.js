import React, {useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {formatDate, generateUniqueId} from '../configuration/utils';
import ContainerScrollView from '../components/ContainerScrollView';
import {
  deleteTask,
  getTaskLists,
  getTasksWithFilter,
  saveTask,
  updateTask,
} from '../services/task';
import Feather from 'react-native-vector-icons/Feather';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Modalize} from 'react-native-modalize';

const initialValues = {
  codeTask: generateUniqueId(),
  taskName: '',
  taskDate: new Date(),
  done: false,
};

function TasksScreen(props) {
  const [tasks, setTasks] = useState([]);
  const [lists, setLists] = useState([]);
  const [shadow, setShadow] = useState(false);
  const [datePickerVisible, setDatePickerVisible] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [filter, setFilter] = useState(false);
  const [dataList] = useState(props.route.params.data);
  const [task, setTask] = useState(initialValues);
  const modalizeRef = useRef(null);

  const onOpen = item => {
    modalizeRef.current?.open();
    setTask({
      ...task,
      codeTask: item.codeTask,
      taskName: item.taskName,
      taskDate: new Date(item.taskDate),
      done: item.done,
    });
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const handleFocus = () => {
    setShadow(true);
  };

  const handleBlur = () => {
    setShadow(false);
  };

  const showDatePicker = () => {
    setDatePickerVisible(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisible(false);
  };

  const handleConfirm = date => {
    hideDatePicker();
    setTask({...task, ['taskDate']: date});
  };

  const saveTaskData = () => {
    const data = {
      key: selectedIndex + 1,
      task: task,
    };
    saveTask(data).then(res => {
      if (res) {
        setShadow(false);
        setTask(initialValues);
        getTasks(dataList ? dataList.key : null, filter);
      }
    });
  };

  const updateTaskData = () => {
    updateTask(task).then(res => {
      if (res) {
        onClose();
        setTask(initialValues);
        getTasks(dataList ? dataList.key : null, filter);
      }
    });
  };

  const deleteTaskData = codeTask => {
    deleteTask(codeTask).then(res => {
      if (res) {
        getTasks(dataList ? dataList.key : null, filter);
      }
    });
  };

  const taskFilterData = flagFilter => {
    getTasks(dataList ? dataList.key : null, flagFilter);
  };

  const handlePress = index => {
    setSelectedIndex(index);
  };

  const getTasks = (listCode, flagFilter) => {
    getTasksWithFilter(listCode, flagFilter).then(res => {
      setTasks(res);
    });
  };

  const getLists = () => {
    getTaskLists().then(res => {
      setLists(res);
    });
  };

  useEffect(() => {
    if (dataList) setSelectedIndex(dataList.key - 1);
    getTasks(dataList ? dataList.key : null, filter);
    getLists();
  }, []);

  return (
    <View style={[styles.viewMain]}>
      {/*{shadow && <View style={[styles.viewShadow]} />}*/}
      <DateTimePickerModal
        date={task.taskDate}
        isVisible={datePickerVisible}
        mode="datetime"
        minimumDate={new Date()}
        is24Hour
        locale="es_PE"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
      {tasks.length > 0 && (
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            taskFilterData(!filter);
            setFilter(!filter);
          }}
          style={[styles.touchFilter]}>
          <Feather name={filter ? 'square' : 'check-square'} size={15} />
          <Text style={[styles.touchFilterText]}>
            {filter ? 'Ver todas las tareas' : 'Ver solo tareas completadas'}
          </Text>
        </TouchableOpacity>
      )}
      <ContainerScrollView {...props}>
        <View style={[styles.viewContainer]}>
          {tasks.map((task, index) => {
            return (
              <View
                key={index}
                style={[styles.viewContainerTask]}>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => onOpen(task)}
                  style={[styles.viewContainerTaskLeft]}>
                  <Feather
                    name={task.done ? 'check-square' : 'square'}
                    size={20}
                    color={'#E1E1E1FF'}
                  />
                  <View style={[styles.viewContainerTaskLeftView]}>
                    <Text style={task.done && {color: '#c5c5c5'}}>
                      {task.taskName}
                    </Text>
                    <Text style={task.done && {color: '#c5c5c5'}}>
                      {formatDate(new Date(task.taskDate))}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => deleteTaskData(task.codeTask)}>
                  <Feather name="x" size={20} color={'#E1E1E1FF'} />
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </ContainerScrollView>
      <View>
        {shadow && (
          <View>
            <View style={[styles.viewDate]}>
              <Text style={[styles.viewDateText]}>
                {'Fecha: ' + formatDate(task.taskDate)}
              </Text>
            </View>
            <View style={[styles.viewLists]}>
              {lists.map((list, index) => {
                return (
                  <TouchableOpacity
                    activeOpacity={0.9}
                    key={index}
                    style={[
                      styles.listItems,
                      index === selectedIndex && {backgroundColor: '#FC7B27'},
                    ]}
                    onPress={() => handlePress(index)}>
                    <Text style={index === selectedIndex && {color: 'white'}}>
                      {list.listName}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          </View>
        )}
        <View style={[styles.viewSearch]}>
          <View style={[styles.viewSearchLeft]}>
            <TextInput
              style={[styles.viewSearchText]}
              placeholder="Yo quiero ..."
              value={task.taskName}
              placeholderTextColor={'#a1a1a1'}
              onChangeText={value => {
                setTask({...task, ['taskName']: value});
              }}
              onFocus={handleFocus}
              onBlur={handleBlur}
            />
            {shadow && (
              <TouchableOpacity
                style={[styles.touchIcon]}
                onPress={() => showDatePicker()}>
                <Feather name="calendar" size={20} color={'#a1a1a1'} />
              </TouchableOpacity>
            )}
          </View>
          <TouchableOpacity
            activeOpacity={0.9}
            style={[
              styles.touchIconSearch,
              task.taskName &&
                task.taskDate &&
                selectedIndex !== null && {backgroundColor: '#FC7B27'},
            ]}
            onPress={() =>
              task.taskName &&
              task.taskDate &&
              selectedIndex !== null &&
              saveTaskData()
            }>
            <Feather name="arrow-up" size={25} color={'white'} />
          </TouchableOpacity>
        </View>
      </View>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={true}
        withHandle={false}>
        <View style={[styles.viewModalize]}>
          <View
            style={[styles.viewModalizeHeader]}>
            <TouchableOpacity onPress={() => updateTaskData()}>
              <Text style={[styles.viewModalizeHeaderText]}>Guardar</Text>
            </TouchableOpacity>
          </View>
          <TextInput
            style={[styles.viewModalizeTextInput]}
            placeholder="Yo quiero ..."
            value={task.taskName}
            placeholderTextColor={'#a1a1a1'}
            onChangeText={value => {
              setTask({...task, ['taskName']: value});
            }}
          />
          <TouchableOpacity
            activeOpacity={0.9}
            style={[styles.viewModalizeDate]}
            onPress={() => showDatePicker()}>
            <Feather name="calendar" size={20} />
            <Text style={[styles.viewModalizeDateText]}>
              {formatDate(task.taskDate)}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => setTask({...task, ['done']: !task.done})}
            style={[styles.viewModalizeDone]}>
            <Feather name={task.done ? 'check-square' : 'square'} size={20} />
            <Text style={[styles.viewModalizeDoneText]}>
              Marcar como terminado
            </Text>
          </TouchableOpacity>
        </View>
      </Modalize>
    </View>
  );
}

export default TasksScreen;

const styles = StyleSheet.create({
  viewMain: {flex: 1, backgroundColor: 'white', paddingHorizontal: 10},
  viewShadow: {
    backgroundColor: 'rgba(1, 1, 1, 0.3)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    // zIndex: 1,
  },
  viewDate: {
    paddingHorizontal: 20,
    paddingVertical: 8,
    backgroundColor: '#FC7B27',
    borderRadius: 25,
    marginBottom: 10,
  },
  viewDateText: {color: 'white', fontSize: 14},
  listItems: {
    backgroundColor: '#f4f4f4',
    borderRadius: 25,
    paddingHorizontal: 15,
    paddingVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 5,
  },
  viewSearch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewSearchLeft: {
    flexDirection: 'row',
    flex: 1,
    position: 'relative',
  },
  viewSearchText: {
    fontSize: 16,
    backgroundColor: '#f4f4f4',
    borderRadius: 25,
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#0000001f',
    flex: 1,
  },
  touchIcon: {position: 'absolute', top: 25, right: 25},
  touchIconSearch: {
    backgroundColor: '#D7D7D7FF',
    borderRadius: 50,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
  },
  touchFilter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  touchFilterText: {fontSize: 14, marginLeft: 5},
  viewContainer: {paddingTop: 20},
  viewContainerTask: {
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#E1E1E1FF',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  viewContainerTaskLeft: {flexDirection: 'row', alignItems: 'center'},
  viewContainerTaskLeftView: {marginLeft: 10},
  viewLists: {flexDirection: 'row'},
  viewModalize: {paddingHorizontal: 20, paddingVertical: 20},
  viewModalizeHeader: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
  viewModalizeHeaderText: {fontSize: 16, color: '#FC7B27'},
  viewModalizeTextInput: {
    fontSize: 20,
    marginVertical: 20,
    paddingVertical: 10,
  },
  viewModalizeDate: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  viewModalizeDateText: {fontSize: 16, marginLeft: 5},
  viewModalizeDone: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  viewModalizeDoneText: {fontSize: 16, marginLeft: 5},
});
