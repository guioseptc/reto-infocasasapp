import React, {useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {deviceWidth, formatDate} from '../configuration/utils';
import ContainerScrollView from '../components/ContainerScrollView';
import {searchTask} from '../services/task';

function SearchScreen(props) {
  const [taskSearch, setTaskSearch] = useState('');
  const [tasks, setTasks] = useState([]);
  const taskSearchRef = useRef();

  const searchByText = text => {
    searchTask(text).then(res => {
      setTasks(res);
    });
  };

  useEffect(() => {
    taskSearchRef.current.focus();
    searchByText('');
  }, []);

  return (
    <View style={[styles.viewMain]}>
      <View style={[styles.viewSearch]}>
        <View style={[styles.viewSearchTouch]}>
          <Feather name="search" size={20} color={'#a1a1a1'} />
          <TextInput
            ref={taskSearchRef}
            style={[styles.viewSearchText]}
            placeholder="Buscar tareas ..."
            value={taskSearch}
            onChangeText={value => {
              setTaskSearch(value);
              searchByText(value);
            }}
          />
        </View>
        <TouchableOpacity activeOpacity={0.9} style={[styles.touchBack]} onPress={() => props.navigation.goBack()}>
          <Text>Cancelar</Text>
        </TouchableOpacity>
      </View>
      <ContainerScrollView {...props}>
        <View style={[styles.viewContainer]}>
          {tasks.map((task, index) => {
            return (
              <View key={index} style={[styles.viewContainerTask]}>
                <Feather
                  name={task.done ? 'check-square' : 'square'}
                  size={20}
                  color={'#E1E1E1FF'}
                />
                <View style={[styles.viewContainerTaskView]}>
                  <Text style={task.done && {color: '#c5c5c5'}}>
                    {task.taskName}
                  </Text>
                  <Text style={task.done && {color: '#c5c5c5'}}>
                    {formatDate(new Date(task.taskDate))}
                  </Text>
                </View>
              </View>
            );
          })}
        </View>
      </ContainerScrollView>
    </View>
  );
}

export default SearchScreen;

const styles = StyleSheet.create({
  viewMain: {flex: 1, backgroundColor: 'white', paddingHorizontal: 10},
  viewSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  viewSearchTouch: {
    backgroundColor: 'rgba(0, 0, 0, 0.03)',
    borderRadius: 25,
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 4,
    flexDirection: 'row',
    alignItems: 'center',
    width: deviceWidth * 0.6,
  },
  viewSearchText: {color: '#a1a1a1', marginLeft: 10, fontSize: 16},
  viewContainer: {paddingTop: 20},
  viewContainerTask: {
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#E1E1E1FF',
    alignItems: 'center',
    marginBottom: 10,
  },
  viewContainerTaskView: {marginLeft: 10},
  touchBack: {flex: .8, height: 30, alignItems: "center", justifyContent: "center"},
});
