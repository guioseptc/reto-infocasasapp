
# Reto React Native - InfoCasasApp

Esta es una versión inicial del reto planteado por Infocasas para postular como React developer

## Pre-requisitos

Debe tener instalada las siguientes versiones de los programas

* Node 17
* npm 8.1
* JDK, se puede descargar desde [este enlace](https://www.oracle.com/java/technologies/javase-downloads.html.)
* React Native cli, se puede instalar mediante este comando ### `npm install -g react-native-cli`

Para ejecutar un proyecto de React Native de manera local en un dispositivo Android, 
es necesario tener instalado el entorno de desarrollo de Android Studio en tu sistema. 
Por otro lado, si deseas ejecutar el proyecto en un dispositivo iOS, es necesario tener 
instalado el entorno de desarrollo de Xcode en tu sistema. [Mas información](https://reactnative.dev/docs/environment-setup)

## Comandos para levantar el proyecto

### `npm install` o `npm i`
Primero instalamos todas las dependencias del proyecto

### `npm run start`
Iniciamos el servidor de desarrollo en una terminal

### `npm run android`
Abrimos otra terminal sin cerrar la anterior para compilar y ejecutar la 
aplicación en un dispositivo Android conectado o en un emulador de Android local que se tenga abierto. 

### `npm run ios`
Abrimos otra terminal sin cerrar la anterior para compilar y ejecutar la aplicación en un 
dispositivo iOS conectado o en un simulador de iOS.

## Comandos adicionales (Opcionales)

### `gradlew assembleRelease`
Si desean probar en un celular que no este conectado o emulado, ejecutamos este comando 
situándonos en la carpeta android/ del proyecto para generar un apk que se puede instalar 
en cualquier dispositivo android

### `gradlew bundleRelease`
Ejecutamos este comando situándonos en la carpeta android/ del proyecto para generar un aab (Solo si se desea llevar el app a la Play Store)

## Autor

Guiosep Tunqui C.

* [Linkedin](https://www.linkedin.com/in/gtunqui/)
* [Gitlab](https://gitlab.com/guioseptc)
